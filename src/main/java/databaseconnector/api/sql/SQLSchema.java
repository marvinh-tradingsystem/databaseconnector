package databaseconnector.api.sql;

import databaseconnector.api.Schema;

import java.util.Set;

/**
 * This describes a schema for an SQL database
 */
public interface SQLSchema extends Schema {
    Set<SQLTable> getTables();
}

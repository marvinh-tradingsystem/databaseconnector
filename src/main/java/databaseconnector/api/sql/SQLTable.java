package databaseconnector.api.sql;

import databaseconnector.api.Table;
import databaseconnector.api.sql.constraint.Constraint;

import java.util.Set;

/**
 * A table in a schema for an SQL database.
 */
public interface SQLTable extends Table {
    Set<Constraint> getConstraints();
}

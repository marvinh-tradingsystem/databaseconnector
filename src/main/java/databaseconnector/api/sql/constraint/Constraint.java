package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

public interface Constraint {
    /**
     * @return The column this constraint is applied to
     */
    Column getConstrainedColumn();
}

package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

import java.util.Objects;

public class Check implements Constraint {
    private final Column column;
    private final String condition;

    public Check(Column column, String condition) {
        assert column != null;
        assert condition != null;

        this.column = column;
        this.condition = condition;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    public String getCondition() {
        return condition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Check check = (Check) o;
        return column.equals(check.column) &&
                condition.equals(check.condition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, condition);
    }
}

package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

import java.util.Objects;

public class Unique implements Constraint {
    private final Column column;

    public Unique(Column column) {
        assert column != null;
        this.column = column;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unique unique = (Unique) o;
        return column.equals(unique.column);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column);
    }
}

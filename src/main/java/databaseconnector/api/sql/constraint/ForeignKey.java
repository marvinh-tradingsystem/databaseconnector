package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;
import databaseconnector.api.sql.SQLTable;

import java.util.Objects;

public class ForeignKey implements Constraint {
    private final Column column;
    private final SQLTable referenceTable; //the table this foreign key references to
    private final Column referenceColumn;  //the column this foreign key references to

    public ForeignKey(Column column, SQLTable referenceTable, Column referenceColumn) {
        assert column != null;
        assert referenceTable != null;
        assert referenceColumn != null;

        this.column = column;
        this.referenceTable = referenceTable;
        this.referenceColumn = referenceColumn;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    public SQLTable getReferenceTable() {
        return referenceTable;
    }

    public Column getReferenceColumn() {
        return referenceColumn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ForeignKey that = (ForeignKey) o;
        return column.equals(that.column) &&
                referenceTable.equals(that.referenceTable) &&
                referenceColumn.equals(that.referenceColumn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, referenceTable, referenceColumn);
    }
}

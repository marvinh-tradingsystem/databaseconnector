package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

import java.util.Objects;

public class NotNull implements Constraint {
    private final Column column;

    public NotNull(Column column) {
        assert column != null;
        this.column = column;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotNull notNull = (NotNull) o;
        return column.equals(notNull.column);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column);
    }
}

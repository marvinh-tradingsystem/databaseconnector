package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

import java.util.Objects;

public class Default implements Constraint {
    private final Column column;
    private final String statement;

    public Default(Column column, String statement) {
        assert column != null;
        assert statement != null;

        this.column = column;
        this.statement = statement;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    public String getStatement() {
        return statement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Default aDefault = (Default) o;
        return column.equals(aDefault.column) &&
                statement.equals(aDefault.statement);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column, statement);
    }
}

package databaseconnector.api.sql.constraint;

import databaseconnector.api.Column;

import java.util.Objects;

public class PrimaryKey implements Constraint {
    private final Column column;

    public PrimaryKey(Column column) {
        assert column != null;
        this.column = column;
    }

    @Override
    public Column getConstrainedColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimaryKey that = (PrimaryKey) o;
        return column.equals(that.column);
    }

    @Override
    public int hashCode() {
        return Objects.hash(column);
    }
}

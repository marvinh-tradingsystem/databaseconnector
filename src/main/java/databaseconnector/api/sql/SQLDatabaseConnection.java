package databaseconnector.api.sql;

import databaseconnector.api.Column;
import databaseconnector.api.DatabaseConnection;
import databaseconnector.api.exception.ConstraintViolationException;
import databaseconnector.api.exception.TableNotExistsException;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static databaseconnector.api.sql.SQLDatabaseConnection.Row;

/**
 * A connection to SQL databases
 */
public interface SQLDatabaseConnection extends DatabaseConnection<SQLSchema, Row> {
    class TableColumn {
        private final SQLTable table;
        private final Column column;
        private final Value value;

        public TableColumn(SQLTable table, Column column, Value value) {
            this.table = table;
            this.column = column;
            this.value = value;
        }

        public SQLTable getTable() {
            return table;
        }

        public Column getColumn() {
            return column;
        }

        public Value getValue() {
            return value;
        }
    }

    class Row {
        private final Map<Column, Value> columnValues;
        private final SQLTable table;

        public Row(Map<Column, Value> columnValues, SQLTable table) {
            assert columnValues != null;
            assert table != null;
            this.columnValues = columnValues;
            this.table = table;
        }

        public Optional<Value> get(Column column){
            assert column != null;
            Optional<Map.Entry<Column, Value>> columnValueEntry = columnValues.entrySet().stream().filter(columnValueEntry1 -> columnValueEntry1.getKey().getName().equalsIgnoreCase(column.getName())
                    && columnValueEntry1.getKey().getDatatype().equalsIgnoreCase(column.getDatatype())).findAny();
            return columnValueEntry.map(Map.Entry::getValue);
        }

        public Set<Column> getColumns(){
            return columnValues.keySet();
        }

        public SQLTable getTable() {
            return table;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Row row = (Row) o;
            if (!(columnValues.size() == row.columnValues.size())){
                return false;
            }
            return columnValues.entrySet().stream().allMatch(
                    columnValueEntry -> row.columnValues.entrySet().stream().anyMatch(columnValueEntry2 ->
                    columnValueEntry.getKey().getName().equalsIgnoreCase(columnValueEntry2.getKey().getName())
                    && columnValueEntry.getValue().get().equals(columnValueEntry2.getValue().get())
                    && table.getName().equalsIgnoreCase(((Row) o).getTable().getName())));
        }

        @Override
        public int hashCode() {
            return Objects.hash(columnValues);
        }

        @Override
        public String toString() {
            return "Row{" +
                    "columnValues=" + columnValues +
                    '}';
        }
    }

    @Override
    void insert(Row data) throws ConstraintViolationException;

    Set<Column> getColumns(String tableName) throws TableNotExistsException;
}

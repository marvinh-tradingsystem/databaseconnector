package databaseconnector.api;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Connects to a database
 * @param <S> The type of the schema the database has to fulfill.
 *           For an SQL database S should be an instance of SQLSchema.
 * @param <D> Type of the data that is to be stored in the database.
 */
public interface DatabaseConnection<S extends Schema, D> {
    class Value{
        private final String value;

        public Value(String value) {
            assert value != null;
            this.value = value;
        }

        public String get(){
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Value value1 = (Value) o;
            return value.equals(value1.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return "Value{" +
                    "value='" + value + '\'' +
                    '}';
        }
    }

    /**
     * Checks if the database matches the given schema
     * @param schema The schema to check the database against
     * @return True, if the database matches the given schema.
     *         False if not.
     */
    boolean isInitiated(S schema);

    /**
     * Builds up the database according to the given schema.
     * All data stored in the database will be deleted.
     * @param schema The schema the database shall meet
     */
    void init(S schema);

    /**
     * Inserts a row into the table.
     * @param data The data to be inserted
     */
    void insert(D data) throws Exception;

    /**
     * Updates entries in the database
     * @param data the row specifying the values to be set
     * @param predicate Predicate that should match before updating.
     *                   The values specified in data will be set for each
     *                   record that matches all predicates.
     */
    void update(D data, Predicate<D> predicate);

    /** Deletes all records that match the given predicates
     * @param predicate The predicate to be matched.
     */
    void delete(Predicate<D> predicate);

    /**
     * Reads data from the database.
     * If one or more predicates are given only data that matches
     * the predicates will be returned.
     * @param predicate Predicate the data should be matched against.
     * @return Stream of data that has been found
     */
    Stream<D> read(Predicate<D> predicate);

    /**
     * Registers a listener that will be called each time the data in the
     * database changes
     * @param dataChangeListener Listener to be called when data changes
     */
    void registerDataChangeListener(DataChangeListener<S, D> dataChangeListener);

    /**
     * Unregisters a DataChangeListener
     * @param dataChangeListener Listener to be unregistered
     */
    void unregisterDataChangeListener(DataChangeListener<S, D> dataChangeListener);
}

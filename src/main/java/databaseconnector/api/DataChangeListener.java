package databaseconnector.api;

/**
 * Handles changes to the database
 * @param <S> The type of the schema the database fulfills.
 * @param <D> Type of the data that is to be stored in the database.
 */
public interface DataChangeListener<S extends Schema, D> {
    void initiated(S newSchema);
    void inserted(D insertedData);
    void deleted(D deletedData);
    void updated(D oldData, D newData);
}

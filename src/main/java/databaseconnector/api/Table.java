package databaseconnector.api;

import java.util.Set;

/**
 * A table in a table-based database schema
 */
public interface Table {
    String getName();
    Set<Column> getColumns();
}

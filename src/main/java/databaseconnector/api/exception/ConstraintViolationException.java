package databaseconnector.api.exception;

public class ConstraintViolationException extends Exception{
    public ConstraintViolationException(String message) {
        super(message);
    }

    public ConstraintViolationException(Throwable cause) {
        super(cause);
    }
}

package databaseconnector.api.exception;

public class TableNotExistsException extends Exception{
    public TableNotExistsException(String message) {
        super(message);
    }
}

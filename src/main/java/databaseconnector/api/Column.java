package databaseconnector.api;

/**
 * A column in a table-based database schema
 */
public interface Column {
    String getName();
    String getDatatype();
}

package databaseconnector.impl;

import java.io.File;

/**
 * Connects to a persistent H2 database. If it does not run yet, it will be started.
 */
public class H2PersistentDatabaseConnection extends H2DatabaseConnection {
    private final File databaseFile;

    /**
     * @param databaseFile File where the H2 database is stored
     */
    public H2PersistentDatabaseConnection(File databaseFile) {
        assert databaseFile != null;
        this.databaseFile = databaseFile;
    }

    @Override
    protected String buildDatabaseUrl(){
        return String.format("jdbc:h2:%s", databaseFile.toString());
    }
}

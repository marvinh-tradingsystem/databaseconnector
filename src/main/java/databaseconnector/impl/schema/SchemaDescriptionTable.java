package databaseconnector.impl.schema;

import databaseconnector.api.Column;
import databaseconnector.api.sql.SQLTable;
import databaseconnector.api.sql.constraint.Constraint;
import databaseconnector.api.sql.constraint.NotNull;
import databaseconnector.api.sql.constraint.PrimaryKey;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SchemaDescriptionTable implements SQLTable {
    public static final Column ID = new Column() {
        @Override
        public String getName() {
            return "ID";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(510)";
        }
    };
    public static final Column TABLE = new Column() {
        @Override
        public String getName() {
            return "table_name";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };
    public static final Column COLUMN = new Column() {
        @Override
        public String getName() {
            return "column_name";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };
    public static final Column DATATYPE = new Column() {
        @Override
        public String getName() {
            return "datatype";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    @Override
    public String getName() {
        return "_SCHEMA";
    }

    @Override
    public Set<Column> getColumns() {
        return new HashSet<>(Arrays.asList(
                ID,
                TABLE,
                COLUMN,
                DATATYPE
        ));
    }

    @Override
    public Set<Constraint> getConstraints() {
        return new HashSet<>(Arrays.asList(
                new PrimaryKey(ID),
                new NotNull(TABLE),
                new NotNull(COLUMN),
                new NotNull(DATATYPE)
        ));
    }
}

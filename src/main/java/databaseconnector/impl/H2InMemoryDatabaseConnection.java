package databaseconnector.impl;

/**
 * Connects to the H2 in-memory database. If it does not run yet, it will be started.
 */
public class H2InMemoryDatabaseConnection extends H2DatabaseConnection {

    private final String databaseName;

    /**
     * @param databaseName Name of the database to connect to
     */
    public H2InMemoryDatabaseConnection(String databaseName) {
        assert databaseName != null;
        this.databaseName = databaseName;
    }

    @Override
    protected String buildDatabaseUrl(){
        return String.format("jdbc:h2:mem:%s;DB_CLOSE_DELAY=-1", databaseName);
    }
}

module database_connector {
    requires java.sql;

    exports databaseconnector.api;
    exports databaseconnector.api.exception;
    exports databaseconnector.api.sql;
    exports databaseconnector.api.sql.constraint;
    exports databaseconnector.impl;
}
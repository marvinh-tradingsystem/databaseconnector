package databaseconnector.impl;

import databaseconnector.api.Column;
import databaseconnector.api.DataChangeListener;
import databaseconnector.api.DatabaseConnection.Value;
import databaseconnector.api.exception.ConstraintViolationException;
import databaseconnector.api.exception.TableNotExistsException;
import databaseconnector.api.sql.SQLDatabaseConnection;
import databaseconnector.api.sql.SQLDatabaseConnection.Row;
import databaseconnector.api.sql.SQLSchema;
import databaseconnector.api.sql.SQLTable;
import databaseconnector.api.sql.constraint.Constraint;
import databaseconnector.api.sql.constraint.NotNull;
import databaseconnector.api.sql.constraint.PrimaryKey;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class H2InMemoryDatabaseConnectionTest {
    private static class CarsTable implements SQLTable{
        @Override
        public Set<Constraint> getConstraints() {
            return new HashSet<>(Arrays.asList(
                    new PrimaryKey(Name),
                    new NotNull(Age)
            ));
        }

        @Override
        public String getName() {
            return "Cars";
        }

        @Override
        public Set<Column> getColumns() {
            return new HashSet<>(Arrays.asList(
                    Name,
                    Age,
                    Doors
            ));
        }

        private Column Doors = new Column() {
            @Override
            public String getName() {
                return "Doors";
            }

            @Override
            public String getDatatype() {
                return "INT";
            }
        };
        private Column Name = new Column() {
            @Override
            public String getName() {
                return "Name";
            }

            @Override
            public String getDatatype() {
                return "VARCHAR(255)";
            }
        };
        private Column Age = new Column() {
            @Override
            public String getName() {
                return "Age";
            }

            @Override
            public String getDatatype() {
                return "INT";
            }
        };
    }

    private class TestSchema implements SQLSchema {
        @Override
        public Set<SQLTable> getTables() {
            return new HashSet<>(Collections.singletonList(
                    Cars
            ));
        }

        public CarsTable Cars = new CarsTable();
    }

    private static class PersonTable implements SQLTable{

        @Override
        public Set<Constraint> getConstraints() {
            return Collections.singleton(new PrimaryKey(Name));
        }

        @Override
        public String getName() {
            return "Person";
        }

        public static Column Name = new Column() {
            @Override
            public String getName() {
                return "name";
            }

            @Override
            public String getDatatype() {
                return "VARCHAR(255)";
            }
        };

        public static Column FirstName = new Column() {
            @Override
            public String getName() {
                return "first name";
            }

            @Override
            public String getDatatype() {
                return "VARCHAR(255)";
            }
        };

        public static Column Age = new Column() {
            @Override
            public String getName() {
                return "age";
            }

            @Override
            public String getDatatype() {
                return "BIGINT";
            }
        };

        @Override
        public Set<Column> getColumns() {
            return new HashSet<>(Arrays.asList(
                    Name,
                    FirstName,
                    Age
            ));
        }
    }

    private class TestSchema2 implements SQLSchema {
        @Override
        public Set<SQLTable> getTables() {
            return new HashSet<>(Collections.singletonList(
                    Persons
            ));
        }

        public PersonTable Persons = new PersonTable();
    }

    private final TestSchema schema = new TestSchema();
    private final TestSchema2 schema2 = new TestSchema2();

    private class ChangeListener implements DataChangeListener<SQLSchema, Row>{
        public String last = "";

        @Override
        public void initiated(SQLSchema newSchema) {
            last = "initiated";
        }

        @Override
        public void inserted(Row insertedData) {
            last = "inserted";
        }

        @Override
        public void deleted(Row deletedData) {
            last = "deleted";
        }

        @Override
        public void updated(Row oldData, Row newData) {
            last = "updated";
        }
    }

    @Test
    void getColumns() throws TableNotExistsException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);
        Set<Column> columns = databaseConnection.getColumns(schema.Cars.getName());
        assertTrue(columns.stream().allMatch(column -> {
            Optional<Column> schemaColumn = schema.Cars.getColumns().stream()
                    .filter(schemaColumn12 -> schemaColumn12.getName().equalsIgnoreCase(column.getName())
                            && schemaColumn12.getDatatype().equalsIgnoreCase(column.getDatatype()))
                    .findAny();
            return schemaColumn.isPresent();
        }));
        columns = databaseConnection.getColumns(schema2.Persons.getName());
        assertTrue(columns.stream().allMatch(column -> {
            Optional<Column> schemaColumn = schema2.Persons.getColumns().stream()
                    .filter(schemaColumn1 -> schemaColumn1.getName().equalsIgnoreCase(column.getName())
                            && schemaColumn1.getDatatype().equalsIgnoreCase(column.getDatatype()))
                    .findAny();
            return schemaColumn.isPresent();
        }));
    }

    @Test
    void init() {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);
        databaseConnection.init(schema);
    }

    @Test
    void isInitiated() {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(Collections::emptySet);
        assertFalse(databaseConnection.isInitiated(schema));
        databaseConnection.init(schema);
        assertTrue(databaseConnection.isInitiated(schema));
        databaseConnection.init(schema);
        assertTrue(databaseConnection.isInitiated(schema));
        assertFalse(databaseConnection.isInitiated(schema2));
        databaseConnection.init(schema2);
        assertTrue(databaseConnection.isInitiated(schema2));
    }

    @Test
    void delete() throws ConstraintViolationException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);

        Map<Column, Value> columnValues1 = new HashMap<>();
        columnValues1.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues1.put(schema.Cars.Age, new Value("4"));
        columnValues1.put(schema.Cars.Doors, new Value("2"));
        Row row1 = new Row(columnValues1, schema.Cars);
        databaseConnection.insert(row1);

        Map<Column, Value> columnValues2 = new HashMap<>();
        columnValues2.put(schema.Cars.Name, new Value("BMW X3"));
        columnValues2.put(schema.Cars.Age, new Value("8"));
        columnValues2.put(schema.Cars.Doors, new Value("4"));
        Row row2 = new Row(columnValues2, schema.Cars);
        databaseConnection.insert(row2);

        Map<Column, Value> columnValues3 = new HashMap<>();
        columnValues3.put(schema.Cars.Name, new Value("Audi A8"));
        columnValues3.put(schema.Cars.Age, new Value("2"));
        Row row3 = new Row(columnValues3, schema.Cars);
        databaseConnection.insert(row3);

        Map<Column, Value> columnValues4 = new HashMap<>();
        columnValues4.put(schema.Cars.Name, new Value("BMW X4"));
        columnValues4.put(schema.Cars.Age, new Value("10"));
        columnValues4.put(schema.Cars.Doors, new Value("4"));
        Row row4 = new Row(columnValues4, schema.Cars);
        databaseConnection.insert(row4);

        List<Row> data = databaseConnection.read(row ->
                row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())
        ).collect(Collectors.toList());

        Map<Column, Value> columnValuesA = new HashMap<>();
        columnValuesA.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValuesA.put(schema.Cars.Age, new Value("4"));
        columnValuesA.put(schema.Cars.Doors, new Value("2"));
        Row assertRow1 = new Row(columnValuesA, schema.Cars);

        Map<Column, Value> columnValuesB = new HashMap<>();
        columnValuesB.put(schema.Cars.Name, new Value("BMW X3"));
        columnValuesB.put(schema.Cars.Age, new Value("8"));
        columnValuesB.put(schema.Cars.Doors, new Value("4"));
        Row assertRow2 = new Row(columnValuesB, schema.Cars);

        Map<Column, Value> columnValuesC = new HashMap<>();
        columnValuesC.put(schema.Cars.Name, new Value("Audi A8"));
        columnValuesC.put(schema.Cars.Age, new Value("2"));
        columnValuesC.put(schema.Cars.Doors, new Value(""));
        Row assertRow3 = new Row(columnValuesC, schema.Cars);

        Map<Column, Value> columnValuesD = new HashMap<>();
        columnValuesD.put(schema.Cars.Name, new Value("BMW X4"));
        columnValuesD.put(schema.Cars.Age, new Value("10"));
        columnValuesD.put(schema.Cars.Doors, new Value("4"));
        Row assertRow4 = new Row(columnValuesD, schema.Cars);

        assertTrue(data.contains(assertRow1));
        assertTrue(data.contains(assertRow2));
        assertTrue(data.contains(assertRow3));
        assertTrue(data.contains(assertRow4));

        databaseConnection.delete(row -> {
            if (!row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())){
                return false;
            }
            assertTrue(row.get(schema.Cars.Name).isPresent());
            return row.get(schema.Cars.Name).get().get().startsWith("BMW");
        });

        data = databaseConnection.read(row ->
                row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())
        ).collect(Collectors.toList());

        assertTrue(data.contains(assertRow1));
        assertFalse(data.contains(assertRow2));
        assertTrue(data.contains(assertRow3));
        assertFalse(data.contains(assertRow4));
    }

    @Test
    void update() throws ConstraintViolationException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);

        Map<Column, Value> columnValues1 = new HashMap<>();
        columnValues1.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues1.put(schema.Cars.Age, new Value("4"));
        columnValues1.put(schema.Cars.Doors, new Value("2"));
        Row row1 = new Row(columnValues1, schema.Cars);
        databaseConnection.insert(row1);

        Map<Column, Value> columnValues2 = new HashMap<>();
        columnValues2.put(schema.Cars.Name, new Value("BMW X3"));
        columnValues2.put(schema.Cars.Age, new Value("8"));
        columnValues2.put(schema.Cars.Doors, new Value("4"));
        Row row2 = new Row(columnValues2, schema.Cars);
        databaseConnection.insert(row2);

        Map<Column, Value> columnValues3 = new HashMap<>();
        columnValues3.put(schema.Cars.Name, new Value("Audi A8"));
        columnValues3.put(schema.Cars.Age, new Value("2"));
        Row row3 = new Row(columnValues3, schema.Cars);
        databaseConnection.insert(row3);

        Map<Column, Value> columnValues4 = new HashMap<>();
        columnValues4.put(schema.Cars.Name, new Value("BMW X4"));
        columnValues4.put(schema.Cars.Age, new Value("10"));
        columnValues4.put(schema.Cars.Doors, new Value("4"));
        Row row4 = new Row(columnValues4, schema.Cars);
        databaseConnection.insert(row4);

        List<Row> data = databaseConnection.read(row ->
                row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())
        ).collect(Collectors.toList());

        Map<Column, Value> columnValuesA = new HashMap<>();
        columnValuesA.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValuesA.put(schema.Cars.Age, new Value("4"));
        columnValuesA.put(schema.Cars.Doors, new Value("2"));
        Row assertRow1 = new Row(columnValuesA, schema.Cars);

        Map<Column, Value> columnValuesB = new HashMap<>();
        columnValuesB.put(schema.Cars.Name, new Value("BMW X3"));
        columnValuesB.put(schema.Cars.Age, new Value("8"));
        columnValuesB.put(schema.Cars.Doors, new Value("4"));
        Row assertRow2 = new Row(columnValuesB, schema.Cars);

        Map<Column, Value> columnValuesC = new HashMap<>();
        columnValuesC.put(schema.Cars.Name, new Value("Audi A8"));
        columnValuesC.put(schema.Cars.Age, new Value("2"));
        columnValuesC.put(schema.Cars.Doors, new Value(""));
        Row assertRow3 = new Row(columnValuesC, schema.Cars);

        Map<Column, Value> columnValuesD = new HashMap<>();
        columnValuesD.put(schema.Cars.Name, new Value("BMW X4"));
        columnValuesD.put(schema.Cars.Age, new Value("10"));
        columnValuesD.put(schema.Cars.Doors, new Value("4"));
        Row assertRow4 = new Row(columnValuesD, schema.Cars);

        Map<Column, Value> columnValues5 = new HashMap<>();
        columnValues5.put(schema.Cars.Name, new Value("BMW X10"));
        columnValues5.put(schema.Cars.Age, new Value("10"));
        columnValues5.put(schema.Cars.Doors, new Value("4"));
        Row row5 = new Row(columnValues5, schema.Cars);

        Map<Column, Value> columnValuesE = new HashMap<>();
        columnValuesE.put(schema.Cars.Name, new Value("BMW X10"));
        columnValuesE.put(schema.Cars.Age, new Value("10"));
        columnValuesE.put(schema.Cars.Doors, new Value("4"));
        Row assertRow5 = new Row(columnValuesE, schema.Cars);

        assertTrue(data.contains(assertRow1));
        assertTrue(data.contains(assertRow2));
        assertTrue(data.contains(assertRow3));
        assertTrue(data.contains(assertRow4));
        assertFalse(data.contains(assertRow5));

        databaseConnection.update(row5, row -> {
            if (!row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())){
                return false;
            }
            assertTrue(row.get(schema.Cars.Name).isPresent());
            return row.get(schema.Cars.Name).get().get().equals("BMW X3");
        });

        data = databaseConnection.read(row ->
                row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())
        ).collect(Collectors.toList());

        assertTrue(data.contains(assertRow1));
        assertFalse(data.contains(assertRow2));
        assertTrue(data.contains(assertRow3));
        assertTrue(data.contains(assertRow4));
        assertTrue(data.contains(assertRow5));
    }

    @Test
    void insertAndReadTable() throws ConstraintViolationException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);

        Map<Column, Value> columnValues1 = new HashMap<>();
        columnValues1.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues1.put(schema.Cars.Age, new Value("4"));
        columnValues1.put(schema.Cars.Doors, new Value("2"));
        Row row1 = new Row(columnValues1, schema.Cars);
        databaseConnection.insert(row1);

        Map<Column, Value> columnValues2 = new HashMap<>();
        columnValues2.put(schema.Cars.Name, new Value("BMW X3"));
        columnValues2.put(schema.Cars.Age, new Value("8"));
        columnValues2.put(schema.Cars.Doors, new Value("4"));
        Row row2 = new Row(columnValues2, schema.Cars);
        databaseConnection.insert(row2);

        Map<Column, Value> columnValues3 = new HashMap<>();
        columnValues3.put(schema.Cars.Name, new Value("Audi A8"));
        columnValues3.put(schema.Cars.Age, new Value("2"));
        Row row3 = new Row(columnValues3, schema.Cars);
        databaseConnection.insert(row3);

        Map<Column, Value> columnValues4 = new HashMap<>();
        columnValues4.put(schema.Cars.Name, new Value("BMW X4"));
        columnValues4.put(schema.Cars.Age, new Value("10"));
        columnValues4.put(schema.Cars.Doors, new Value("4"));
        Row row4 = new Row(columnValues4, schema.Cars);
        databaseConnection.insert(row4);

        List<Row> data = databaseConnection.read(row ->
                row.getTable().getName().equalsIgnoreCase(schema.Cars.getName())
        ).collect(Collectors.toList());

        Map<Column, Value> columnValuesA = new HashMap<>();
        columnValuesA.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValuesA.put(schema.Cars.Age, new Value("4"));
        columnValuesA.put(schema.Cars.Doors, new Value("2"));
        Row assertRow1 = new Row(columnValuesA, schema.Cars);

        Map<Column, Value> columnValuesB = new HashMap<>();
        columnValuesB.put(schema.Cars.Name, new Value("BMW X3"));
        columnValuesB.put(schema.Cars.Age, new Value("8"));
        columnValuesB.put(schema.Cars.Doors, new Value("4"));
        Row assertRow2 = new Row(columnValuesB, schema.Cars);

        Map<Column, Value> columnValuesC = new HashMap<>();
        columnValuesC.put(schema.Cars.Name, new Value("Audi A8"));
        columnValuesC.put(schema.Cars.Age, new Value("2"));
        columnValuesC.put(schema.Cars.Doors, new Value(""));
        Row assertRow3 = new Row(columnValuesC, schema.Cars);

        Map<Column, Value> columnValuesD = new HashMap<>();
        columnValuesD.put(schema.Cars.Name, new Value("BMW X4"));
        columnValuesD.put(schema.Cars.Age, new Value("10"));
        columnValuesD.put(schema.Cars.Doors, new Value("4"));
        Row assertRow4 = new Row(columnValuesD, schema.Cars);

        assertTrue(data.contains(assertRow1));
        assertTrue(data.contains(assertRow2));
        assertTrue(data.contains(assertRow3));
        assertTrue(data.contains(assertRow4));
    }

    @Test
    void insertDuplicatePrimaryKey() throws ConstraintViolationException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        databaseConnection.init(schema);

        Map<Column, Value> columnValues1 = new HashMap<>();
        columnValues1.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues1.put(schema.Cars.Age, new Value("4"));
        columnValues1.put(schema.Cars.Doors, new Value("2"));
        Row row1 = new Row(columnValues1, schema.Cars);
        databaseConnection.insert(row1);

        Map<Column, Value> columnValues2 = new HashMap<>();
        columnValues2.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues2.put(schema.Cars.Age, new Value("8"));
        columnValues2.put(schema.Cars.Doors, new Value("4"));
        Row row2 = new Row(columnValues2, schema.Cars);
        assertThrows(ConstraintViolationException.class, () -> databaseConnection.insert(row2));
    }

    @Test
    void registerUnregisterDataChangeListener() throws ConstraintViolationException {
        final SQLDatabaseConnection databaseConnection = new H2InMemoryDatabaseConnection("test");
        ChangeListener changeListener = new ChangeListener();
        assertTrue(changeListener.last.isEmpty());
        databaseConnection.registerDataChangeListener(changeListener);
        assertTrue(changeListener.last.isEmpty());

        databaseConnection.init(schema);
        assertEquals("initiated", changeListener.last);

        changeListener.last = "";
        Map<Column, Value> columnValues1 = new HashMap<>();
        columnValues1.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues1.put(schema.Cars.Age, new Value("4"));
        columnValues1.put(schema.Cars.Doors, new Value("2"));
        Row row1 = new Row(columnValues1, schema.Cars);
        databaseConnection.insert(row1);
        assertEquals("inserted", changeListener.last);

        changeListener.last = "";
        Map<Column, Value> columnValues2 = new HashMap<>();
        columnValues2.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues2.put(schema.Cars.Age, new Value("4"));
        columnValues2.put(schema.Cars.Doors, new Value("2"));
        Row row2 = new Row(columnValues2, schema.Cars);
        databaseConnection.update(row2, row -> row.getTable().getName().equalsIgnoreCase(schema.Cars.getName()));
        assertEquals("updated", changeListener.last);

        changeListener.last = "";
        databaseConnection.delete(row -> row.getTable().getName().equalsIgnoreCase(schema.Cars.getName()));
        assertEquals("deleted", changeListener.last);

        changeListener.last = "";
        databaseConnection.unregisterDataChangeListener(changeListener);
        databaseConnection.init(schema);
        assertTrue(changeListener.last.isEmpty());

        Map<Column, Value> columnValues3 = new HashMap<>();
        columnValues3.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues3.put(schema.Cars.Age, new Value("4"));
        columnValues3.put(schema.Cars.Doors, new Value("2"));
        Row row3 = new Row(columnValues3, schema.Cars);
        databaseConnection.insert(row3);
        assertTrue(changeListener.last.isEmpty());

        Map<Column, Value> columnValues4 = new HashMap<>();
        columnValues4.put(schema.Cars.Name, new Value("Dodge Challenger SRT Hellcat"));
        columnValues4.put(schema.Cars.Age, new Value("4"));
        columnValues4.put(schema.Cars.Doors, new Value("2"));
        Row row4 = new Row(columnValues4, schema.Cars);
        databaseConnection.update(row4, row -> row.getTable().getName().equalsIgnoreCase(schema.Cars.getName()));
        assertTrue(changeListener.last.isEmpty());
    }
}
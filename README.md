# DatabaseConnector
This is a maven artifact which abstracts the logic for accessing databases so that the user of this maven artifact easily can change database implementations without modifying his source code.

## Running tests
When you run unit tests with Intellij you have to delegate the execution to maven.
This can be done by clicking "File" -> "Settings" -> "Build, Execution, Deployments" -> "Build Tools" -> "Maven" -> "Runner" and check "Delegate IDE build/run actions to maven".